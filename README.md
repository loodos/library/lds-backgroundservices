# Background Services 

## What is it

Background Services.

## Full Sample

//TODO: write description

## Who is using this

//TODO: add projects which is using this

## Commercial Support

//TODO: write description

## Contributing

//TODO: write description

## License

Loodos Background Services is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/library/lds-backgroundservices/blob/master/LICENSE)

## Acknowledgements

- [ASP.NET Core](https://github.com/aspnet)

Thanks for providing free open source licensing

- [NCrontab](https://github.com/atifaziz/NCrontab)
